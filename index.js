const createTabs = (arr) => {
    const tabs = document.createElement("ul");
    tabs.classList.add("tabs");

    const tabsContent = document.createElement("ul");
    tabsContent.classList.add("tabs-content");

    arr.forEach(([tab, tabContent], i) => {
        const tabElement = document.createElement("li");
        tabElement.classList.add("tabs-title");
        if (i === 0) {
            tabElement.classList.add("active");
        }
        tabElement.innerHTML = tab;

        tabElement.addEventListener('click', event => {
            const allTabs = document.querySelectorAll('.tabs-title');
            for(let j = 0; j < allTabs.length; j += 1){
                const el = allTabs[j];
                el.classList.remove('active');
            } 
            tabElement.classList.add('active');
            
            const allTabsContent = document.querySelectorAll('.tabs-content-title');
            for(let j = 0; j < allTabsContent.length; j += 1){
                const el = allTabsContent[j];
                el.classList.remove('show');
                if (j === i) {
                    el.classList.add('show');
                } else {
                    el.classList.remove('show');
                }
            } 
        })
        


        tabs.appendChild(tabElement);
        
        const tabContentElement = document.createElement("li");
        
        tabContentElement.classList.add("tabs-content-title");

        if (i === 0) {
            tabContentElement.classList.add("show");
        }

        tabContentElement.innerHTML = tabContent;
        tabsContent.appendChild(tabContentElement);
    })

    const content = document.getElementById('content-id');

    content.appendChild(tabs);
    content.appendChild(tabsContent);
}

const arr = [
    ['Akali','Abandoning the Kinkou Order and her title of the Fist of Shadow, Akali now strikes alone, ready to be the deadly weapon her people need. Though she holds onto all she learned from her master Shen, she has pledged to defend Ionia from its enemies, one kill at a time. Akali may strike in silence, message will be heard loud and clear: fear the assassin with no maste'],
    ['Anivia','Anivia is a benevolent winged spirit who endures endless cycles of life, death, and rebirth to protect the Freljord. A demigod born of unforgiving ice and bitter winds, she wields those elemental powers to thwart any who dare disturb her homeland. Anivia guides and protects the tribes of the harsh north, who revere her as a symbol of hope, and a portent of great change. She fights with every ounce of her being, knowing that through her sacrifice, her memory will endure, and she will be reborn into a new tomorrow.'],
    ['Draven','In Noxus, warriors known as reckoners face one another in arenas where blood is spilled and strength tested—but none has ever been as celebrated as Draven. A former soldier, he found that the crowds uniquely appreciated his flair for the dramatic, not to mention the spray of blood from each of his spinning axes. Addicted to the spectacle of his own brash perfection, Draven has sworn to defeat whomever he must to ensure that his name is chanted throughout the empire forever more.'],
    ['Garen','A proud and noble soldier, Garen fights at the head of the Dauntless Vanguard. He is popular among his fellows, and respected well enough by his enemies—not least as a scion of the prestigious Crownguard family, entrusted with defending Demacia and its ideals. Clad in magic-resistant armor and bearing a mighty broadsword, Garen stands ready to confront mages and sorcerers on the field of battle, in a veritable whirlwind of righteous steel.'],
    ['Katarina','Decisive in judgment and lethal in combat, Katarina is a Noxian assassin of the highest caliber. Eldest daughter to the legendary General Du Couteau, she made her talents known with swift kills against unsuspecting enemies. Her fiery ambition has driven her to pursue heavily-guarded targets, even at the risk of endangering her allies—but no matter the mission, Katarina will not hesitate to execute her duty amid a whirlwind of serrated daggers.'],
]

createTabs(arr);